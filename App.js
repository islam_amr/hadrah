// package import
import React from 'react';
import {View, StatusBar, Text} from 'react-native';
import {Provider} from 'react-redux';

//constants import
import colors from './src/constants/colors';

// Main Navigator import
import MainNavigation from './src/navigation/MainNavigation';

// redux store
import configureStore from './src/redux/configureStore';

const store = configureStore();

const App = () => {
  return (
    <>
      <StatusBar backgroundColor={'#c3922c'} />
      <Provider store={store}>
        <MainNavigation />
      </Provider>
    </>
  );
};

export default App;
