// package import
import React, {useReducer, useEffect} from 'react';
import {View, TextInput, StyleSheet, Text} from 'react-native';
import {useTheme} from '@react-navigation/native';
// constants import
import dimensions from '../../constants/dimensions';
import responsiveFont from '../../constants/responsiveFont';

const UPDATE_INPUT = 'UPDATE_INPUT';
const INPUT_BLUR = 'INPUT_BLUR';
const INPUT_FOCUSED = 'INPUT_FOCUSED';

const inputReducer = (state, action) => {
  switch (action.type) {
    case UPDATE_INPUT:
      return {
        ...state,
        value: action.value,
        isValid: action.isValid,
      };
    case INPUT_BLUR:
      return {
        ...state,
        touched: true,
        focused: false,
      };
    case INPUT_FOCUSED:
      return {
        ...state,
        focused: true,
      };
    default:
      return state;
  }
};
const Input = props => {
  const {myColors} = useTheme();
  const [inputState, dispatchInputState] = useReducer(inputReducer, {
    value: '',
    isValid: false,
    touched: false,
    focused: false,
  });

  const inputChangeHandler = text => {
    const emailReg =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    let isValid = true;
    if (props.number && isNaN(text)) {
      isValid = false;
    }
    if (props.email && !emailReg.test(text)) {
      isValid = false;
    }
    if (props.required && text.trim().length === 0) {
      isValid = false;
    }
    if (props.min != null && +text.length < props.min) {
      isValid = false;
    }
    if (props.max != null && +text.length > props.max) {
      isValid = false;
    }
    dispatchInputState({type: UPDATE_INPUT, value: text, isValid: isValid});
  };

  const lostFocushandler = () => {
    dispatchInputState({type: INPUT_BLUR});
  };

  const inputFocused = () => {
    dispatchInputState({type: INPUT_FOCUSED});
  };

  useEffect(() => {
    props.onInputChange(props.id, inputState.value, inputState.isValid);
  }, [inputState]);

  return (
    <View style={styles.inputCon}>
      <TextInput
        style={[
          styles.input,
          {backgroundColor: myColors.cardBackground, color: myColors.textColor},
        ]}
        placeholderTextColor={myColors.textColor}
        placeholder={props.label}
        value={inputState.value}
        onChangeText={inputChangeHandler}
        onBlur={lostFocushandler}
        onFocus={inputFocused}
      />
      {!inputState.isValid && props.errMsg && inputState.touched && (
        <Text
          style={[
            styles.errMsg,
            {
              flexDirection: 'row',
              width: '85%',
              alignSelf: 'center',
            },
          ]}>
          {props.errMsg}
        </Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  inputCon: {
    flex: 1,
  },
  input: {
    height: dimensions.height * 0.1,
    paddingLeft: '5%',
    fontSize: responsiveFont(18),
    borderRadius: 10,
  },
});

export default Input;
