import {useTheme} from '@react-navigation/native';
import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

const Button = props => {
  const {myColors} = useTheme();
  return (
    <TouchableOpacity
      onPress={props.onPress}
      activeOpacity={0.8}
      style={[
        styles.btnCon,
        {...props.style},
        {backgroundColor: myColors.primary},
      ]}>
      <Text style={props.textStyle}>{props.text}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  btnCon: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
  },
});
export default Button;
