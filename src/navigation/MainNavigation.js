// package import
import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {NavigationContainer, useTheme} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {useSelector} from 'react-redux';

// localization import
import '../i18n';

// screens import
// Auth Screens
import LaunchScreen from '../screens/LaunchScreen';
import LoginScreen from '../screens/LoginScreen';

// App Screens
import HomeScreen from '../screens/HomeScreen';
import CartScreen from '../screens/CartScreen';
import FavoriteScreen from '../screens/FavoriteScreen';
import ProfileScreen from '../screens/ProfileScreen';
import CheckOutScreen from '../screens/CheckOutScreen';
import ProductScreen from '../screens/ProductScreen';

// theme import
import {customDarkTheme, customLightTheme} from '../constants/colors';
import dimensions from '../constants/dimensions';
import responsiveFont from '../constants/responsiveFont';
import ProductDetails from '../screens/ProductDetails';
import {useTranslation} from 'react-i18next';
import SettingsScreen from '../screens/SettingsScreen';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const ProductStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerTransparent: true}}>
      <Stack.Screen name={'Products'} component={ProductScreen} />
      <Stack.Screen
        name={'Product Details'}
        component={ProductDetails}
        options={{
          headerTransparent: false,
        }}
      />
    </Stack.Navigator>
  );
};

const ProfileStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerTransparent: true}}>
      <Stack.Screen name={'Profile'} component={ProfileScreen} />
      <Stack.Screen name={'Settings'} component={SettingsScreen} />
    </Stack.Navigator>
  );
};
const TabStack = () => {
  const {myColors} = useTheme();
  const {t, i18n} = useTranslation();

  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: myColors.primary,
        inactiveTintColor: myColors.textColor,
        style: {
          backgroundColor: myColors.tabBackground,
          height: dimensions.height * 0.09,
        },
        labelStyle: {
          fontSize: responsiveFont(13),
        },
      }}>
      <Tab.Screen
        name={'Home'}
        component={HomeScreen}
        options={{
          title: t('Home'),
          tabBarIcon: ({color}) => (
            <Image
              style={[styles.tabIcon, {tintColor: color}]}
              source={require('../assets/icons/home.png')}
            />
          ),
        }}
      />
      <Tab.Screen
        name={'Products'}
        component={ProductStack}
        options={{
          title: t('Products'),
          tabBarIcon: ({color}) => (
            <Image
              style={[styles.tabIcon, {tintColor: color}]}
              source={require('../assets/icons/products.png')}
            />
          ),
        }}
      />
      <Tab.Screen
        name={'Cart'}
        component={CartScreen}
        options={{
          title: t('Cart'),
          tabBarIcon: ({color}) => (
            <Image
              style={[styles.tabIcon, {tintColor: color}]}
              source={require('../assets/icons/cart.png')}
            />
          ),
        }}
      />
      <Tab.Screen
        name={'Favorite'}
        component={FavoriteScreen}
        options={{
          title: t('Favorite'),
          tabBarIcon: ({color}) => (
            <Image
              style={[styles.tabIcon, {tintColor: color}]}
              source={require('../assets/icons/favorite.png')}
            />
          ),
        }}
      />
      <Tab.Screen
        name={'Profile'}
        component={ProfileStack}
        options={{
          title: t('Profile'),
          tabBarIcon: ({color}) => (
            <Image
              style={[styles.tabIcon, {tintColor: color}]}
              source={require('../assets/icons/profile.png')}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const AuthStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name={'Launch'} component={LaunchScreen} />
      <Stack.Screen name={'Login'} component={LoginScreen} />
    </Stack.Navigator>
  );
};

const MainNavigation = () => {
  const theme = useSelector(state => state.theme);
  const userLoggedIn = useSelector(state => state.user.isLoggedIn);

  return (
    <NavigationContainer theme={theme ? customDarkTheme : customLightTheme}>
      {userLoggedIn ? <TabStack /> : <AuthStack />}
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  tabIcon: {
    width: '50%',
    height: '50%',
    resizeMode: 'contain',
  },
});

export default MainNavigation;
