import {useTheme} from '@react-navigation/native';
import React, {useLayoutEffect} from 'react';
import {
  StyleSheet,
  Text,
  Image,
  FlatList,
  TouchableOpacity,
  View,
} from 'react-native';
import commonStyles from '../constants/commonStyles';
import dimensions from '../constants/dimensions';
import fonts from '../constants/fonts';
import responsiveFont from '../constants/responsiveFont';
import {useTranslation} from 'react-i18next';

const ProductScreen = ({navigation}) => {
  const {myColors} = useTheme();
  const {t, i18n} = useTranslation();
  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: () => (
        <Text style={[styles.headerTitle, {color: myColors.textColor}]}>
          Categories
        </Text>
      ),
    });
  }, [myColors]);
  return (
    <View
      style={[commonStyles.screnView, {backgroundColor: myColors.background}]}>
      <View style={{marginTop: '20%'}}>
        <View
          style={{
            width: '90%',
            backgroundColor: myColors.primary,
            height: dimensions.height * 0.15,
            alignSelf: 'center',
            borderRadius: 20,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={styles.best}>{t('Best Seller')}</Text>
          <View
            style={{
              position: 'absolute',
              top: 0,
              right: '5%',
              width: '30%',
              height: '50%',
            }}>
            <Image
              source={require('../assets/images/htW.png')}
              style={{width: '100%', height: '100%', resizeMode: 'stretch'}}
            />
          </View>
        </View>
      </View>
      <View
        style={{
          flex: 1,
          marginTop: '5%',
        }}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          data={[t('Perfumes'), t('Incense'), t('Oud'), t('Wood Oud')]}
          contentContainerStyle={{paddingHorizontal: '5%'}}
          keyExtractor={item => item}
          renderItem={({item}) => {
            return (
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate('Product Details', {title: item})
                }
                key={item}
                activeOpacity={0.8}
                style={{
                  width: '100%',
                  height: dimensions.height * 0.15,
                  marginBottom: dimensions.height * 0.04,
                  borderRadius: 20,
                  overflow: 'hidden',
                  flexDirection: 'row',
                  backgroundColor: myColors.tabBackground,
                }}>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                  }}>
                  <Text style={[styles.catName, {color: myColors.textColor}]}>
                    {item}
                  </Text>
                  <View
                    style={{
                      position: 'absolute',
                      top: 0,
                      right: '5%',
                      width: '50%',
                      height: '50%',
                    }}>
                    <Image
                      source={require('../assets/images/htW.png')}
                      style={{
                        tintColor: myColors.primary,
                        width: '100%',
                        height: '100%',
                        resizeMode: 'stretch',
                      }}
                    />
                  </View>
                </View>
                <View style={{flex: 1}}>
                  <Image
                    style={{resizeMode: 'cover', width: '100%', height: '100%'}}
                    source={require('../assets/images/homeImg.jpg')}
                  />
                </View>
              </TouchableOpacity>
            );
          }}
        />
      </View>
    </View>
  );
};

export default ProductScreen;

const styles = StyleSheet.create({
  headerTitle: {
    fontSize: responsiveFont(18),
    fontFamily: fonts.ebrima,
    textAlign: 'center',
  },
  best: {
    fontSize: responsiveFont(24),
    fontFamily: fonts.ebrima,
  },
  catName: {
    marginHorizontal: '10%',
    fontSize: responsiveFont(20),
    fontFamily: fonts.ebrima,
  },
});
