// package import
import React, {useState} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  Switch,
  Text,
  KeyboardAvoidingView,
  View,
  FlatList,
} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {useTranslation} from 'react-i18next';

// constants import
import colors from '../constants/colors';
import commonStyles from '../constants/commonStyles';
import dimensions from '../constants/dimensions';
import fonts from '../constants/fonts';
import responsiveFont from '../constants/responsiveFont';
import Button from '../components/ui/Button';

const CartScreen = ({navigation}) => {
  const {myColors} = useTheme();
  const {t, i18n} = useTranslation();

  return (
    <View
      style={[commonStyles.screnView, {backgroundColor: myColors.background}]}>
      <View style={styles.headerLogo}>
        <Image
          style={styles.logoImg}
          source={require('../assets/images/hT.png')}
        />
      </View>
      <View style={{marginTop: '5%', paddingHorizontal: '5%'}}>
        <Text style={[styles.loginTxt, {color: myColors.textColor}]}>
          {t('My Cart')}
        </Text>
      </View>
      <View style={{flex: 0.75, marginTop: '5%'}}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          data={['Perfumes', 'Incense', 'Oud', 'Wood Oud']}
          contentContainerStyle={{paddingHorizontal: '5%'}}
          keyExtractor={item => item}
          renderItem={({item}) => {
            return (
              <View
                key={item}
                activeOpacity={0.8}
                style={{
                  width: '100%',
                  height: dimensions.height * 0.15,
                  marginBottom: dimensions.height * 0.04,
                  borderRadius: 20,
                  overflow: 'hidden',
                  flexDirection: 'row',
                  backgroundColor: myColors.cartCard,
                }}>
                <View style={{flex: 0.3}}>
                  <Image
                    style={{resizeMode: 'cover', width: '100%', height: '100%'}}
                    source={require('../assets/images/homeImg.jpg')}
                  />
                </View>
                <View style={{flex: 0.7, padding: '2.5%'}}>
                  <Text style={[styles.proName, {color: myColors.textColor}]}>
                    {t('Ali Ghazal')}
                  </Text>
                  <Text style={[styles.size, {color: myColors.textColor}]}>
                    {t('40 ML')}
                  </Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      marginTop: '5%',
                    }}>
                    <View
                      style={{
                        flex: 0.4,
                        flexDirection:
                          i18n.language === 'ar' ? 'row-reverse' : 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                      }}>
                      <TouchableOpacity
                        style={[
                          styles.addRemoBtn,
                          {backgroundColor: myColors.cartCard},
                        ]}>
                        <Text
                          style={{
                            fontSize: responsiveFont(18),
                            color: myColors.textColor,
                          }}>
                          -
                        </Text>
                      </TouchableOpacity>
                      <Text
                        style={{
                          fontSize: responsiveFont(18),
                          color: myColors.textColor,
                        }}>
                        1
                      </Text>
                      <TouchableOpacity
                        style={[
                          styles.addRemoBtn,
                          {backgroundColor: myColors.cartCard},
                        ]}>
                        <Text
                          style={{
                            fontSize: responsiveFont(18),
                            color: myColors.textColor,
                          }}>
                          +
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={{flex: 0.6, alignItems: 'flex-end'}}>
                      <Text style={[styles.size, {color: myColors.textColor}]}>
                        {t('120 QAR')}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            );
          }}
        />
      </View>
      <View
        style={{
          flex: 0.25,
          justifyContent: 'space-around',
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'flex-end',
            paddingHorizontal: '5%',
          }}>
          <Text
            style={{fontSize: responsiveFont(14), color: myColors.textColor}}>
            {t('Total Amount')}:
          </Text>
          <Text
            style={{fontSize: responsiveFont(18), color: myColors.textColor}}>
            {t('2000 QAR')}
          </Text>
        </View>
        <View
          style={{height: dimensions.height * 0.1, paddingHorizontal: '5%'}}>
          <Button
            text={t('Check out')}
            style={{marginTop: '5%', paddingVertical: '2.5%'}}
            textStyle={[styles.loginTxt2, {color: 'white'}]}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  headerLogo: {
    flex: 0.15,
    width: '50%',
    alignSelf: 'center',
  },
  logoImg: {
    width: '100%',
    height: '100%',
    resizeMode: 'stretch',
  },
  backBtnCon: {
    width: dimensions.width * 0.1,
    height: dimensions.height * 0.04,
    position: 'absolute',
    top: dimensions.height * 0.1,
    left: dimensions.width * 0.025,
  },
  backImg: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  },
  pickerCon: {
    marginTop: '10%',
    width: '90%',
    alignSelf: 'center',
    flex: 0.1,
    // height: dimensions.height * 0.1,
    borderRadius: 10,
    borderWidth: 2,
    justifyContent: 'center',
  },
  loginTxt: {
    fontSize: responsiveFont(24),
    textTransform: 'uppercase',
    fontFamily: fonts.ebrima,
  },
  loginTxt2: {
    fontSize: responsiveFont(18),
    textTransform: 'uppercase',
  },
  proName: {
    fontSize: responsiveFont(18),
    fontFamily: fonts.ebrima,
  },
  size: {
    fontSize: responsiveFont(14),
    fontFamily: fonts.ebrima,
  },
  addRemoBtn: {
    width: dimensions.width * 0.075,
    height: dimensions.width * 0.075,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: (dimensions.width * 0.075) / 2,
    elevation: 8,
  },
  loginTxt2: {
    fontSize: responsiveFont(18),
    textTransform: 'uppercase',
  },
});

export default CartScreen;

// const styles = StyleSheet.create({});
