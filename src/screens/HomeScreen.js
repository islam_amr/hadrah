import {useTheme} from '@react-navigation/native';
import React from 'react';
import {
  StyleSheet,
  FlatList,
  ScrollView,
  Text,
  Image,
  View,
  TouchableOpacity,
} from 'react-native';
import Button from '../components/ui/Button';
import commonStyles from '../constants/commonStyles';
import dimensions from '../constants/dimensions';
import fonts from '../constants/fonts';
import responsiveFont from '../constants/responsiveFont';
import {useTranslation} from 'react-i18next';

const HomeScreen = () => {
  const {myColors} = useTheme();
  const {t, i18n} = useTranslation();

  return (
    <ScrollView
      style={[commonStyles.screnView, {backgroundColor: myColors.background}]}>
      <View style={{height: dimensions.height * 0.7}}>
        <Image
          source={require('../assets/images/homeImg.jpg')}
          style={styles.homeImg}
        />
        <View style={styles.txtCon}>
          <Text style={styles.homeTxt}>{t('Best')}</Text>
          <Text style={styles.homeTxt}>{t('Seller')}</Text>
        </View>
        <View style={styles.btnCon}>
          <Button
            text={t('Check')}
            style={{paddingHorizontal: '15%', paddingVertical: '1.25%'}}
            textStyle={styles.btnTxt}
          />
        </View>
      </View>
      <View>
        <View style={styles.tCon}>
          <Image
            style={styles.tImg}
            source={require('../assets/images/hT.png')}
          />
        </View>
        <View style={styles.headerTxt}>
          <View>
            <Text style={[styles.news, {color: myColors.textColor}]}>
              {t('News')}
            </Text>
            <Text style={[styles.noteTxt, {color: myColors.textColor}]}>
              {t("You've never seen it before!")}
            </Text>
          </View>
          <Text style={[styles.noteTxt, {color: myColors.textColor}]}>
            {t('View all')}
          </Text>
        </View>
      </View>
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={[1, 2, 3, 4, 5, 6, 7]}
        contentContainerStyle={{marginVertical: '5%'}}
        keyExtractor={item => item}
        renderItem={({item}) => {
          return (
            <TouchableOpacity
              key={item}
              activeOpacity={0.8}
              style={{
                width: dimensions.width * 0.4,
                height: dimensions.height * 0.2,
                marginHorizontal: dimensions.width * 0.025,
                borderRadius: 20,
                overflow: 'hidden',
              }}>
              <Image
                style={{resizeMode: 'cover', width: '100%', height: '100%'}}
                source={require('../assets/images/homeImg.jpg')}
              />
              <View
                style={[
                  styles.miniNew,
                  {backgroundColor: myColors.background},
                ]}>
                <Text
                  style={{fontFamily: fonts.ebrima, color: myColors.textColor}}>
                  {t('News')}
                </Text>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </ScrollView>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  homeImg: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
  txtCon: {
    position: 'absolute',
    bottom: '25%',
    left: '5%',
  },
  homeTxt: {
    fontSize: responsiveFont(35),
    color: 'white',
    fontFamily: fonts.ebrima,
  },
  news: {
    fontSize: responsiveFont(24),
    fontFamily: fonts.ebrima,
  },
  btnCon: {
    flex: 0.1,
    position: 'absolute',
    bottom: '12.5%',
    left: '5%',
  },
  tCon: {
    width: '30%',
    alignSelf: 'center',
    position: 'absolute',
    height: dimensions.height * 0.1,
  },
  tImg: {
    width: '100%',
    height: '100%',
    resizeMode: 'stretch',
  },
  btnTxt: {
    color: 'white',
    fontFamily: fonts.ebrima,
    fontSize: responsiveFont(16),
  },
  headerTxt: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal: '5%',
    alignItems: 'center',
    marginTop: '10%',
    fontFamily: fonts.ebrima,
  },
  noteTxt: {
    fontSize: responsiveFont(14),
    fontFamily: fonts.ebrima,
  },
  miniNew: {
    left: '10%',
    top: '10%',
    position: 'absolute',
    paddingHorizontal: '5%',
    paddingVertical: '2.5%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
});
