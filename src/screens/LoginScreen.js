// package import
import React, {useRef, useState} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Pressable,
  Image,
  Switch,
  Text,
  KeyboardAvoidingView,
  View,
} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import {Picker} from '@react-native-picker/picker';
import {useTranslation} from 'react-i18next';
import ActionSheet from 'react-native-actions-sheet';

// theme actions import
import * as themeActions from '../redux/actions/theme';

// user actions import
import * as userActions from '../redux/actions/user';

// components ui import
import Button from '../components/ui/Button';
import Input from '../components/ui/Input';

// constants import
import colors from '../constants/colors';
import commonStyles from '../constants/commonStyles';
import dimensions from '../constants/dimensions';
import fonts from '../constants/fonts';
import responsiveFont from '../constants/responsiveFont';

const LoginScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const {t, i18n} = useTranslation();
  const actionSheetRef = useRef();
  const {myColors} = useTheme();
  const user = useSelector(state => state.user);
  const [selectedReason, setSelectedReason] = useState(null);
  const options = [
    t('Where Did you Know us from'),
    t('Facebook'),
    t('Instagram'),
    t('LinkedIn'),
    t('Others'),
  ];

  const changeThemeHandler = () => {
    dispatch(themeActions.changeTheme());
  };

  const loginHandler = () => {
    dispatch(userActions.login());
  };
  return (
    <>
      <KeyboardAvoidingView style={{flex: 1}}>
        <View
          style={[
            commonStyles.screnView,
            {backgroundColor: myColors.background},
          ]}>
          <View style={styles.headerLogo}>
            <Image
              style={styles.logoImg}
              source={require('../assets/images/hLogo.png')}
            />
          </View>
          <View style={styles.backBtnCon}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Image
                source={require('../assets/icons/back.png')}
                style={[
                  styles.backImg,
                  {
                    tintColor: myColors.textColor,
                    transform: [
                      {rotate: i18n.language === 'ar' ? '180deg' : '0deg'},
                    ],
                  },
                ]}
              />
            </TouchableOpacity>
          </View>
          <View style={{marginTop: '10%', paddingHorizontal: '5%'}}>
            <Text style={styles.loginTxt}>{t('Login')}</Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              actionSheetRef.current?.setModalVisible();
            }}
            style={[styles.pickerCon, {borderColor: myColors.primary}]}>
            <Text style={[styles.wherTxt, {color: myColors.textColor}]}>
              {t('Where Did you Know us from')}
            </Text>
            <Image
              style={{
                width: 30,
                height: 30,
                resizeMode: 'contain',
                tintColor: myColors.textColor,
              }}
              source={require('../assets/icons/down.png')}
            />
          </TouchableOpacity>
          <View
            style={{
              flex: 0.5,
              paddingHorizontal: '5%',
              marginTop: '10%',
            }}>
            <Input
              id={'name'}
              label={t('Name')}
              onInputChange={(i, z, c) => console.log(i, z, c)}
            />
            <Input
              id={'phoneNumber'}
              label={t('Phone Number')}
              onInputChange={(i, z, c) => console.log(i, z, c)}
            />
            <Input
              id={'email'}
              label={t('Email')}
              onInputChange={(i, z, c) => console.log(i, z, c)}
            />
          </View>
          <View
            style={{
              flex: 0.1,
              marginTop: '10%',
              paddingHorizontal: '5%',
            }}>
            <Button
              onPress={loginHandler}
              text={t('Login')}
              textStyle={[
                styles.loginTxt2,
                {color: 'white', paddingVertical: '3%'},
              ]}
            />
          </View>
        </View>
      </KeyboardAvoidingView>
      <ActionSheet ref={actionSheetRef} containerStyle={styles.modalCon}>
        <View
          style={{
            marginTop: '5%',
          }}>
          <Text style={[styles.actionHeaderTxt, {color: myColors.textColor}]}>
            {t('Where Did you Know us from')}
          </Text>
          <View style={{marginVertical: '5%'}}>
            <Pressable
              style={({pressed}) => [
                {
                  backgroundColor: pressed
                    ? myColors.primary
                    : 'rgba(255,255,255,0)',
                },
              ]}>
              {({pressed}) => (
                <Text
                  style={[
                    styles.actionSubTxt,
                    {color: pressed ? 'white' : 'black'},
                  ]}>
                  {t('Al Hazim Mail')}
                </Text>
              )}
            </Pressable>
            <Pressable
              style={({pressed}) => [
                {
                  backgroundColor: pressed
                    ? myColors.primary
                    : 'rgba(255,255,255,0)',
                },
              ]}>
              {({pressed}) => (
                <Text
                  style={[
                    styles.actionSubTxt,
                    {color: pressed ? 'white' : 'black'},
                  ]}>
                  {t('Doha Festival City')}
                </Text>
              )}
            </Pressable>
            <Pressable
              style={({pressed}) => [
                {
                  backgroundColor: pressed
                    ? myColors.primary
                    : 'rgba(255,255,255,0)',
                },
              ]}>
              {({pressed}) => (
                <Text
                  style={[
                    styles.actionSubTxt,
                    {color: pressed ? 'white' : 'black'},
                  ]}>
                  {t('Social Media')}
                </Text>
              )}
            </Pressable>
          </View>
        </View>
      </ActionSheet>
    </>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  headerLogo: {
    flex: 0.15,
    width: '50%',
    alignSelf: 'center',
  },
  logoImg: {
    width: '100%',
    height: '100%',
    resizeMode: 'stretch',
  },
  backBtnCon: {
    width: dimensions.width * 0.1,
    height: dimensions.height * 0.04,
    position: 'absolute',
    top: dimensions.height * 0.1,
    left: dimensions.width * 0.025,
  },
  backImg: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  },
  wherTxt: {
    fontSize: responsiveFont(15),
    textTransform: 'uppercase',
    fontFamily: fonts.ebrima,
  },
  pickerCon: {
    marginTop: '10%',
    width: '90%',
    alignSelf: 'center',
    flex: 0.1,
    // height: dimensions.height * 0.1,
    borderRadius: 10,
    borderWidth: 2,
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: '5%',
    alignItems: 'center',
  },
  loginTxt: {
    fontSize: responsiveFont(24),
    textTransform: 'uppercase',
    fontFamily: fonts.ebrima,
  },
  loginTxt2: {
    fontSize: responsiveFont(18),
    textTransform: 'uppercase',
  },
  actionHeaderTxt: {
    fontSize: responsiveFont(20),
    textAlign: 'center',
  },
  actionSubTxt: {
    fontSize: responsiveFont(16),
    marginVertical: '2%',
    marginHorizontal: '5%',
  },
  modalCon: {
    width: dimensions.width,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
  },
});
