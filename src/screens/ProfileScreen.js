// package import
import React, {useLayoutEffect, useState} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  Switch,
  Text,
  KeyboardAvoidingView,
  View,
} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import {Picker} from '@react-native-picker/picker';
import {useTranslation} from 'react-i18next';

// theme actions import
import * as themeActions from '../redux/actions/theme';

// user actions import
import * as userActions from '../redux/actions/user';

// components ui import
import Button from '../components/ui/Button';
import Input from '../components/ui/Input';

// constants import
import colors from '../constants/colors';
import commonStyles from '../constants/commonStyles';
import dimensions from '../constants/dimensions';
import fonts from '../constants/fonts';
import responsiveFont from '../constants/responsiveFont';

const LoginScreen = ({navigation}) => {
  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => <View style={{flex: 1}} />,
      headerTitle: () => (
        <View style={{flex: 1}}>
          <Text style={[styles.headerTitle, {color: myColors.textColor}]}>
            Profile
          </Text>
        </View>
      ),
      headerRight: () => (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <TouchableOpacity
            onPress={() => navigation.navigate('Settings')}
            style={[styles.rigthIcon, {backgroundColor: myColors.primary}]}>
            <Image
              style={{
                width: '70%',
                tintColor: 'white',
                height: '70%',
                resizeMode: 'contain',
              }}
              source={require('../assets/icons/settings.png')}
            />
          </TouchableOpacity>
        </View>
      ),
    });
  }, [myColors]);
  const dispatch = useDispatch();
  const {t, i18n} = useTranslation();
  const {myColors} = useTheme();
  const [theme, setTheme] = useState(false);
  const changeThemeHandler = () => {
    setTheme(prev => !prev);
    dispatch(themeActions.changeTheme());
  };

  return (
    <View
      style={[commonStyles.screnView, {backgroundColor: myColors.background}]}>
      <View style={{marginTop: '20%'}}>
        <Text style={[styles.userName, {color: myColors.textColor}]}>
          BO Tamem aymam
        </Text>
      </View>
      <View style={[styles.userImgCon, {backgroundColor: myColors.primary}]}>
        <Image
          style={styles.img}
          source={require('../assets/images/homeImg.jpg')}
        />
      </View>
      <View style={styles.txtCon}>
        <Text
          style={{
            fontSize: responsiveFont(18),
            color: myColors.textColor,
            opacity: 0.6,
          }}>
          Saved Balance
        </Text>
        <Text style={{fontSize: responsiveFont(36), color: myColors.textColor}}>
          14950 QR
        </Text>
        <Text
          style={{
            fontSize: responsiveFont(12),
            color: myColors.textColor,
            opacity: 0.6,
          }}>
          Updated 2 mins ago
        </Text>
      </View>
      <View style={styles.btnCon}>
        <Button text={t('View History')} textStyle={[styles.viewHistory]} />
      </View>
    </View>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  headerTitle: {
    fontSize: responsiveFont(18),
    fontFamily: fonts.ebrima,
    textAlign: 'center',
  },
  headerTitle: {
    fontSize: responsiveFont(18),
    fontFamily: fonts.ebrima,
    textAlign: 'center',
  },
  best: {
    fontSize: responsiveFont(24),
    fontFamily: fonts.ebrima,
  },
  catName: {
    marginHorizontal: '10%',
    fontSize: responsiveFont(20),
    fontFamily: fonts.ebrima,
  },
  userName: {
    fontSize: responsiveFont(24),
    fontFamily: fonts.ebrima,
    alignSelf: 'center',
  },
  userImgCon: {
    width: dimensions.width * 0.5,
    height: dimensions.width * 0.5,
    alignSelf: 'center',
    marginTop: '15%',
    borderRadius: (dimensions.width * 0.5) / 2,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    width: dimensions.width * 0.46,
    height: dimensions.width * 0.46,
    borderRadius: (dimensions.width * 0.48) / 2,
    resizeMode: 'cover',
  },
  txtCon: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '10%',
  },
  btnCon: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewHistory: {
    color: 'white',
    fontSize: responsiveFont(22),
    paddingVertical: '3%',
    paddingHorizontal: '5%',
  },
  rigthIcon: {
    width: dimensions.width * 0.1,
    height: dimensions.width * 0.1,
    backgroundColor: 'red',
    marginRight: dimensions.width * 0.05,
    borderRadius: (dimensions.width * 0.1) / 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

{
  /* <Switch
        trackColor={{false: '#767577', true: myColors.primary}}
        thumbColor={myColors.white}
        ios_backgroundColor="#3e3e3e"
        onValueChange={changeThemeHandler}
        value={theme}
        style={{width: 45, height: '100%'}}
      /> */
}

{
  /* <Switch
trackColor={{false: '#767577', true: myColors.primary}}
thumbColor={myColors.white}
ios_backgroundColor="#3e3e3e"
onValueChange={changeThemeHandler}
value={theme}
style={{width: 45, height: '100%'}}
/> */
}

// const [theme, setTheme] = useState(false);
// const changeThemeHandler = () => {
//   setTheme(prev => !prev);
//   dispatch(themeActions.changeTheme());
// };
