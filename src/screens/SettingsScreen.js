import {useTheme} from '@react-navigation/native';
import React, {useState} from 'react';
import {StyleSheet, Switch, Text, View} from 'react-native';
import {useDispatch} from 'react-redux';
import commonStyles from '../constants/commonStyles';
import * as themeActions from '../redux/actions/theme';

const SettingsScreen = () => {
  const [theme, setTheme] = useState(false);
  const dispatch = useDispatch();
  const changeThemeHandler = () => {
    setTheme(prev => !prev);
    dispatch(themeActions.changeTheme());
  };
  const {myColors} = useTheme();
  return (
    <View
      style={[
        commonStyles.screnView,
        {
          backgroundColor: myColors.background,
          justifyContent: 'center',
          alignItems: 'center',
        },
      ]}>
      <Switch
        trackColor={{false: '#767577', true: myColors.primary}}
        thumbColor={myColors.white}
        ios_backgroundColor="#3e3e3e"
        onValueChange={changeThemeHandler}
        value={theme}
        style={{height: '100%'}}
      />
    </View>
  );
};

export default SettingsScreen;

const styles = StyleSheet.create({});

// const [theme, setTheme] = useState(false);
// const changeThemeHandler = () => {
//   setTheme(prev => !prev);
//   dispatch(themeActions.changeTheme());
// };
