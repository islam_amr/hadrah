// packge import
import React, {useCallback, useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  Image,
  I18nManager,
  DevSettings,
  StyleSheet,
} from 'react-native';
import {useTranslation} from 'react-i18next';
import {useTheme} from '@react-navigation/native';
import {Picker} from '@react-native-picker/picker';

// ui components
import Button from '../components/ui/Button';

// constants import
import commonStyles from '../constants/commonStyles';
import fonts from '../constants/fonts';
import responsiveFont from '../constants/responsiveFont';
import dimensions from '../constants/dimensions';

const LaunchScreen = ({navigation}) => {
  const {t, i18n} = useTranslation();
  const {myColors} = useTheme();

  const languageChangeHandler = () => {
    i18n.changeLanguage(i18n.language === 'ar' ? 'en' : 'ar').then(() => {
      I18nManager.forceRTL(i18n.language === 'ar');
      DevSettings.reload();
    });
  };

  const [selectedLanguage, setSelectedLanguage] = useState();
  return (
    <View
      style={[commonStyles.screnView, {backgroundColor: myColors.background}]}>
      <View style={{flex: 1}}>
        <View style={[styles.pickerCon, {backgroundColor: myColors.primary}]}>
          <Picker
            style={{fontFamily: fonts.ebrima, color: 'white'}}
            mode="dropdown"
            dropdownIconColor={'white'}
            selectedValue={selectedLanguage}
            onValueChange={(itemValue, itemIndex) => {
              setSelectedLanguage(itemValue);
              languageChangeHandler(itemValue);
            }}>
            {i18n.language === 'ar'
              ? [t('English'), t('Arabic')].reverse().map(item => {
                  return (
                    <Picker.Item
                      key={item}
                      fontFamily={fonts.ebrima}
                      label={item}
                      value={false}
                    />
                  );
                })
              : [t('Arabic'), t('English')].reverse().map(item => {
                  return (
                    <Picker.Item
                      key={item}
                      fontFamily={fonts.ebrima}
                      label={item}
                      value={false}
                    />
                  );
                })}
          </Picker>
        </View>

        <Image
          source={require('../assets/images/launchLogo.png')}
          style={styles.img}
        />
        <View style={styles.btnCon}>
          <Button
            style={{backgroundColor: myColors.primary}}
            text={t('Login')}
            textStyle={[styles.btnTxt, {color: 'white'}]}
            onPress={() => navigation.navigate('Login')}
            // onPress={handleChangeLang}
          />
        </View>
      </View>
      <View style={{flex: 1}}>
        <Image
          source={require('../assets/images/launch.png')}
          style={styles.img}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  halfCon: {
    flex: 1,
    backgroundColor: 'red',
  },
  img: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
  btnCon: {
    width: '50%',
    height: '10%',
    position: 'absolute',
    alignSelf: 'center',
    bottom: '5%',
  },
  btnTxt: {
    fontSize: responsiveFont(16),
    fontFamily: fonts.ebrima_bold,
    textTransform: 'uppercase',
  },
  pickerCon: {
    position: 'absolute',
    zIndex: 1000,
    top: '2.5%',
    left: '2.5%',
    height: dimensions.height * 0.04,
    width: dimensions.width * 0.3,
    borderRadius: 20,
    justifyContent: 'center',
  },
});

export default LaunchScreen;
