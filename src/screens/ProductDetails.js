import {useTheme} from '@react-navigation/native';
import React, {useLayoutEffect, useState} from 'react';
import {StyleSheet, Image, Text, View, TouchableOpacity} from 'react-native';
import commonStyles from '../constants/commonStyles';
import fonts from '../constants/fonts';
import responsiveFont from '../constants/responsiveFont';
import {useTranslation} from 'react-i18next';
import Button from '../components/ui/Button';
import {Picker} from '@react-native-picker/picker';
import dimensions from '../constants/dimensions';

const ProductDetails = ({navigation, route}) => {
  console.log(route);
  const {myColors} = useTheme();
  const {t, i18n} = useTranslation();
  const [selectedSize, setSelectedSize] = useState(null);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: route.params?.title,
      headerTintColor: 'white',
      headerStyle: {
        backgroundColor: myColors.primary,
      },
    });
  }, [route]);
  return (
    <View
      style={[commonStyles.screnView, {backgroundColor: myColors.background}]}>
      <View
        style={{
          position: 'absolute',
          top: 0,
          width: '20%',
          height: '7.5%',
          alignSelf: 'center',
          zIndex: 1000,
        }}>
        <Image
          source={require('../assets/images/htW.png')}
          style={{
            tintColor: myColors.primary,
            width: '100%',
            height: '100%',
            resizeMode: 'stretch',
          }}
        />
      </View>
      <View style={{flex: 1}}>
        <View
          style={{
            flex: 0.75,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            style={{
              width: '90%',
              alignSelf: 'center',
              height: '90%',
              resizeMode: 'cover',
              borderRadius: 50,
              overflow: 'hidden',
            }}
            source={require('../assets/images/homeImg.jpg')}
          />
          <View
            style={{
              position: 'absolute',
              width: '30%',
              height: '7.5%',
              bottom: '30%',
              right: 0,
              backgroundColor: myColors.primary,
              borderTopLeftRadius: 20,
              borderBottomLeftRadius: 20,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: responsiveFont(16),
                fontFamily: fonts.ebrima,
                color: 'white',
              }}>
              {t('2000 QAR')}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              position: 'absolute',
              height: '7.5%',
              bottom: '7.5%',
              backgroundColor: myColors.background,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 20,
              paddingHorizontal: '2.5%',
            }}>
            {[1, 2, 3, 4, 5].map(item => {
              return (
                <Image
                  key={item}
                  style={{
                    width: 20,
                    height: 20,
                    tintColor: myColors.primary,
                  }}
                  source={require('../assets/icons/star.png')}
                />
              );
            })}
          </View>
        </View>
        <View style={{flex: 0.25}}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 0.8, justifyContent: 'center'}}>
              <View
                style={{
                  backgroundColor: myColors.primary,
                  borderTopRightRadius: 20,
                  borderBottomRightRadius: 20,
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingVertical: '2.5%',
                  width: '90%',
                }}>
                <Text
                  style={{
                    fontSize: responsiveFont(16),
                    fontFamily: fonts.ebrima,
                    color: 'white',
                  }}>
                  {t('2000 QAR')}
                </Text>
              </View>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'space-between',
              }}>
              <Text
                style={{
                  fontSize: responsiveFont(20),
                  fontFamily: fonts.ebrima_bold,
                  color: myColors.textColor,
                  textAlign: 'center',
                }}>
                GEBER
              </Text>
              <View
                style={{
                  borderWidth: 1,
                  borderColor: myColors.primary,
                  borderRadius: 20,
                  height: '50%',
                  justifyContent: 'center',
                }}>
                <Picker
                  style={{
                    fontFamily: fonts.ebrima,
                    color: myColors.textColor,
                  }}
                  mode="dropdown"
                  dropdownIconColor={myColors.textColor}
                  selectedValue={selectedSize}
                  onValueChange={(itemValue, itemIndex) => {
                    setSelectedSize(itemValue);
                  }}>
                  {['Size', '40 ML', '80 ML', '100 ML'].map(item => {
                    return (
                      <Picker.Item
                        style={{fontSize: responsiveFont(18), color: '#222222'}}
                        key={item}
                        label={item}
                        fontFamily={fonts.ebrima}
                        value={item}
                      />
                    );
                  })}
                </Picker>
              </View>
            </View>
            <View
              style={{
                flex: 0.8,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                style={{
                  width: dimensions.width * 0.15,
                  height: dimensions.width * 0.15,
                  borderRadius: (dimensions.width * 0.15) / 2,
                  backgroundColor: myColors.primary,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../assets/icons/heart.png')}
                  style={{
                    width: '60%',
                    height: '60%',
                    tintColor: 'white',
                    resizeMode: 'contain',
                  }}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Button
              text={t('Check out')}
              style={{
                width: '50%',
                alignSelf: 'center',
                paddingVertical: '2.5%',
              }}
              textStyle={[styles.loginTxt2, {color: 'white'}]}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default ProductDetails;

const styles = StyleSheet.create({
  loginTxt2: {
    fontSize: responsiveFont(18),
    textTransform: 'uppercase',
  },
});
