import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import {I18nManager} from 'react-native';

// the translations
// (tip move them in a JSON file and import them)
const resources = {
  ar: {
    translation: {
      Login: 'تسجيل الدخول',
      English: 'English',
      Arabic: 'عربى',
      News: 'جديد',
      Best: 'افضل',
      Seller: 'المبيعات',
      'Best Seller': 'افضل المبيعات',
      Check: 'افحص',
      "You've never seen it before!": 'ما لم تراه من قبل',
      'View all': 'اعرض الكل',
      Perfumes: 'العطور',
      Incense: 'عطور',
      Oud: 'عود',
      'Wood Oud': 'عود خشب',
      'Where Did you Know us from': 'كيف عرفت عنا',
      Facebook: 'فيسبوك',
      Instagram: 'انستجرام',
      LinkedIn: 'لينكدان',
      Others: 'اخرى',
      Name: 'الاسم',
      'Phone Number': 'رقم الهاتف',
      Email: 'بريد الاكترونى',
      'Ali Ghazal': 'على غزل',
      '40 ML': '40 مل',
      '120 QAR': '120 دينار',
      '2000 QAR': '2000 دينار',
      'Total Amount': 'اجمالى السعر',
      'Check out': 'الدفع',
      'My Cart': 'عربة التسوق',
      Home: 'الرايسية',
      Favorite: 'المفضلة',
      Cart: 'العربة',
      Products: 'منتجات',
      Profile: 'حسابى',
    },
  },
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: I18nManager.isRTL ? 'ar' : 'en',

    keySeparator: false, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });

export default i18n;
