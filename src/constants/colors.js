import {DarkTheme, DefaultTheme} from '@react-navigation/native';
const customDarkTheme = {
  ...DarkTheme,
  myColors: {
    background: '#1e1f28',
    primary: '#c3922c',
    textColor: '#f3f3f3',
    lightText: '#9aa2ac',
    cardBackground: '#2a2c36',
    tabBackground: '#1f1f29',
    cartCard: '#2a2d36',
  },
};

const customLightTheme = {
  ...DefaultTheme,
  myColors: {
    background: '#f9f9f9',
    primary: '#c3922c',
    textColor: '#222222',
    lightText: '#8f8e8e',
    cardBackground: '#ffffff',
    tabBackground: '#ffffff',
    cartCard: '#ffffff',
  },
};

export {customDarkTheme, customLightTheme};
