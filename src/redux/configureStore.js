// package import
import {createStore, combineReducers} from 'redux';

// Reducers
import theme from './reducers/theme';
import user from './reducers/user';

const rootReducer = combineReducers({
  theme,
  user,
});

export default configureStore = () => {
  return createStore(rootReducer);
};
