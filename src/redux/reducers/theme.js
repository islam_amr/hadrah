import {CHANGE_THEME} from '../actions/theme';

const initialState = false;

export default (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_THEME:
      return !state;
    default:
      return state;
  }
};
